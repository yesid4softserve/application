# Server Application

This application is a server with the ability to track a `http` request and return a message.

To run this application in your terminal:

* Clone the repository to your local machine with the command `git clone git@gitlab.com:yesid4softserve/application.git` and enter to the folder with the command `cd application`.
  * Execute the command `python3 src/server.py` to run the server.
  * If you want to create an image and run a container that run the server, follow the next commands:
    * docker build -t application .
    * docker run -p 8080:80 -it application
* If you don't want to clone this repository, download the next image and run the container.
  * docker pull yesid4code/application:v1
  * docker run -p 8080:80 -it yesid4code/application:v1
* After following one of the steps avobe, enter the `http://127.0.0.0:8080` URL in your browser and view the result.
