from http.server import HTTPServer, BaseHTTPRequestHandler

hostName = "0.0.0.0"
serverPort = 80

class serverHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(bytes("<html><body><h1>Hello World</h1</body></html>", "utf-8"))


if __name__ == "__main__":
    server = HTTPServer((hostName, serverPort), serverHandler)
    print("Server running")
    print(f'server started http://{hostName}:{serverPort}80')

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass

    server.server_close()
    print("Server stopped!")
